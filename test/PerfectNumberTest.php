<?php

use PHPUnit\Framework\TestCase;

/**
 * @covers PerfectNumberClass
 */
final class PerfectNumberTest extends TestCase
{

	public function testGetExpectedResults() {
		$this->assertEquals(
			'perfect',
		    PerfectNumber::getClassification(6)
		);

		$this->assertEquals(
			'abundant',
		    PerfectNumber::getClassification(12)
		);

		$this->assertEquals(
			'deficient',
		    PerfectNumber::getClassification(8)
		);
	}	

	public function testCannotUseNegativeValues() {
		$this->expectException(InvalidArgumentException::class);

        PerfectNumber::getClassification(-100);
	}
}