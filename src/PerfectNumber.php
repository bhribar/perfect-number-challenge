<?php

final class PerfectNumber
{
	/**
	 * returns 'perfect', 'abundant', or 'deficient'
	 */

	public static function getClassification($integer)
	{
    	if ($integer < 1) {
    		throw new InvalidArgumentException(
    			'Not a positive integer'
    		);
    	}
	
		$sum = 0;
		for ($x = 1; $x < $integer; $x++) {
			if ($integer % $x == 0) {
				$sum += $x;
			}
		}
		
		if ($sum == $integer) {
			return 'perfect';
		} else if ($sum > $integer) {
			return 'abundant';
		} else {
			return 'deficient';
		}
	}
}

